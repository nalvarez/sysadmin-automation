#!/bin/bash

set -ex

vagrant destroy -f
rm -f sanedebian.box
vagrant up
vagrant halt
vagrant box remove sanedebian.box || true
vagrant package --output=sanedebian.box
vagrant box add --name=sanedebian.box sanedebian.box
vagrant destroy -f
rm sanedebian.box
