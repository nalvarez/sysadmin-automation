#!/bin/bash

set -xe

cp /vagrant/90apt-cacher-ng /etc/apt/apt.conf.d/
apt-get update
aptitude remove -y nfs-common
aptitude markauto -y ~i~slibs ~i~sperl ~i~n-common ~i~n^lib rpcbind
aptitude clean
apt-get -y upgrade
aptitude clean
